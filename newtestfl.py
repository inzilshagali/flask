import os
from flask import Flask, flash, request, redirect, url_for, render_template
from flask_autoindex import AutoIndex
from flask_httpauth import HTTPTokenAuth
from werkzeug.utils import secure_filename

WORK_PATH = os.environ.get("MYDIR", os.path.join(os.path.dirname(__file__), 'FlaskShare'))
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)


AutoIndex(app, browse_root=WORK_PATH)
app.config['SECRET_KEY'] = 'a really long secret key, I don`t say it never'

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    render_template('autoindex.html', message="str(auth.current_user())")
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('upload_file',filename=filename))
        else:
            flash('Incorrect format')
            return redirect(request.url)


if __name__ == '__main__':
    app.run()