from flask_login import UserMixin

from myproject import db, manager


class Access(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(20), nullable = False)
    actiontime = db.Column(db.DateTime, nullable = False)
    action = db.Column(db.String(80), nullable = False)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key = True)
    login = db.Column(db.String(20), nullable = False, unique = True)
    password = db.Column(db.String(255), nullable = False)

@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)
