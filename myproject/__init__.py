import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_autoindex import AutoIndex
from flask_login import LoginManager


WORK_PATH = os.environ.get("MYDIR", os.path.join(os.path.dirname(__file__), 'FlaskShare'))
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']= 'postgres://postgres:123@localhost/autoindex'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'a really long secret key, di da bu di da bu day'
app.config['UPLOAD_FOLDER'] = WORK_PATH

db = SQLAlchemy(app)
manager = LoginManager(app)
aui = AutoIndex(app, browse_root=WORK_PATH, add_url_rules=False)

from myproject import models, routes

db.create_all()

