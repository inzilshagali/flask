from flask import Flask, flash, request, redirect, url_for, render_template, session
from flask_autoindex import AutoIndex
from flask_login import login_user, login_required, logout_user
from werkzeug.utils import secure_filename
from werkzeug.security import check_password_hash, generate_password_hash
import datetime
import os

from myproject import app, db, aui, WORK_PATH, ALLOWED_EXTENSIONS
from myproject.models import Access, User


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/lili')
@app.route('/lili/<path:path>')
@login_required
def autoindex(path = '.'):
    return aui.render_autoindex(path)

@app.route('/lili', methods=['GET', 'POST'])
@login_required
def upload_file():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    # if session.get('logged_in'):
    #     print("Hello {}".format(session['logged_in']))
    # render_template('autoindex.html')
    # print(session.get('logged_in'))
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            newAction = Access(username = session['username'], actiontime = now, action = 'unsuccess file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            newAction = Access(username = session['username'], actiontime = now, action = 'unsuccess file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            newAction = Access(username = session['username'], actiontime = now, action = 'added file' + filename)
            db.session.add(newAction)
            db.session.commit()
            return redirect(url_for('upload_file', filename=filename))
        else:
            flash('Incorrect format')
            newAction = Access(username = session['username'], actiontime = now, action = 'incorrect file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
    # else:
    #     return redirect(url_for('login_page'))


@app.route('/login/', methods=['POST', 'GET'])
def login_page():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    message = "Hello"
    render_template('login.html', message=message)
    if request.method == 'POST':
        login = request.form.get('login')
        password = request.form.get('password')
        if login and password:
            user = User.query.filter_by(login = login).first()
            if user and check_password_hash(user.password, password):
                message = "successful login"
                login_user(user)
                session['logged_in'] = True
                session['username'] = login
                newLogin = Access(username = session['username'], actiontime = now, action = 'login')
                db.session.add(newLogin)
                db.session.commit()
                return redirect(url_for('autoindex'))
                # return render_template('autoindex.html', curdir = ".")
            else:
                message = "Login or password is not correct"
                newLogin = Access(username = 'unknow user', actiontime = now, action = 'unsuccessful login')
                db.session.add(newLogin)
                db.session.commit()
        else:
            message = "Please enter login and password"
            newLogin = Access(username = 'unknow user', actiontime = now, action = 'unsuccessful login')
            db.session.add(newLogin)
            db.session.commit()
    return render_template('login.html', message=message)


@app.route('/logout', methods=['POST', 'GET'])
@login_required
def logout():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    newLogout = Access(username = session['username'], actiontime = now, action = 'logout')
    db.session.add(newLogout)
    db.session.commit()
    logout_user()
    session['logged_in'] = False
    return redirect(url_for('login_page'))


@app.route('/register', methods=['POST', 'GET'])
# @auth.login_required
def register():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    message = ""
    login = request.form.get('login')
    password = request.form.get('password')

    if request.method == 'POST':
        if not (login or password):
            message = "Please, fill all fields"
            newRegister = Access(username = 'unknown=' + login, actiontime = now, action = 'unsuccessful register')
            db.session.add(newRegister)
            db.session.commit()
        else:
            hash_passwd = generate_password_hash(password)
            new_user = User(login = login, password = hash_passwd)
            db.session.add(new_user)
            newRegister = Access(username = login, actiontime = now, action = 'new register')
            db.session.add(newRegister)
            db.session.commit()

            return redirect(url_for('login_page'))
    return render_template('register.html', message = message)