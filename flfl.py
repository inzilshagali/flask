import os
from flask import Flask, flash, request, redirect, url_for, render_template, session
from flask_autoindex import AutoIndex
from werkzeug.utils import secure_filename
from flask_sqlalchemy import SQLAlchemy
import datetime

WORK_PATH = os.environ.get("MYDIR", os.path.join(os.path.dirname(__file__), 'FlaskShare'))
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']= 'postgres://postgres:123@localhost/autoindex'
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = 'a really long secret key, di da bu di da bu day'
app.config['UPLOAD_FOLDER'] = WORK_PATH

class Access(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(20), nullable = False)
    actiontime = db.Column(db.DateTime, nullable = False)
    action = db.Column(db.String(80), nullable = False)

tokens = {
    "blablabla": "User",
    os.environ.get("MYPASS"): "MyUser"
}

AutoIndex(app, browse_root=WORK_PATH)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    # if session.get('logged_in'):
    #     print("Hello {}".format(session['logged_in']))
    # render_template('autoindex.html')
    # print(session.get('logged_in'))
    if request.method == 'POST' and session.get('logged_in'):
        if 'file' not in request.files:
            flash('No file part')
            newAction = Access(username = session['username'], actiontime = now, action = 'unsuccess file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            newAction = Access(username = session['username'], actiontime = now, action = 'unsuccess file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            newAction = Access(username = session['username'], actiontime = now, action = 'added file' + filename)
            db.session.add(newAction)
            db.session.commit()
            return redirect(url_for('upload_file', filename=filename))
        else:
            flash('Incorrect format')
            newAction = Access(username = session['username'], actiontime = now, action = 'incorrect file adding')
            db.session.add(newAction)
            db.session.commit()
            return redirect(request.url)
    else:
        return redirect(url_for('login'))


# @auth.verify_token
def verify_token(token):
    # token = request.form.get('token')
    if token in tokens:
        # print(auth.current_user)
        return tokens[token]


@app.route('/login/', methods=['POST', 'GET'])
def login():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    message = "Hello"
    render_template('login.html', message=message)
    if request.method == 'POST':
        token = request.form.get('token')
        if verify_token(token):
            session['logged_in'] = True
            session['username'] = tokens[token]
            newLogin = Access(username = session['username'], actiontime = now, action = 'login')
            db.session.add(newLogin)
            db.session.commit()
            return redirect(url_for('upload_file'))
        else:
            message = "No Access"
            newLogin = Access(username = 'unknow user', actiontime = now, action = 'unsuccessful login')
            db.session.add(newLogin)
            db.session.commit()
    return render_template('login.html', message=message)


@app.route('/logout')
def logout():
    now = datetime.datetime.utcnow().strftime(f"%Y-%m-%d_%H:%M:%S")
    newLogout = Access(username = session['username'], actiontime = now, action = 'logout')
    db.session.add(newLogout)
    db.session.commit()
    # удалить из сессии имя пользователя, если оно там есть
    session['logged_in'] = False
    return redirect(url_for('login'))


@app.route('/secret_page')
# @auth.login_required
def secret_page():
    return "Hello {}".format(session['logged_in'])


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5050)
